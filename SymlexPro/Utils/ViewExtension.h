//
//  Common.h
//  ShasthoNet
//
//  Created by Riaz on 7/25/16.
//  Copyright © 2016 Riaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ViewExtension)
-(void) setBackGroundImage:(NSString *) imageName;
@end
