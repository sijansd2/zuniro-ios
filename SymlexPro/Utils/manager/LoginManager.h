//
//  LoginManager.h
//  SymlexPro
//
//  Created by USER on 8/19/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginManager : NSObject
+(LoginManager*)getSharedInstance;
-(void)signUp:(NSString *)code withPhoneNumber:(NSString *)number withPassword:(NSString *)pass  withEmail:(NSString *)email andWithSuccess:(void (^)(BOOL result))success andWithError:(void (^)(BOOL result))error;
@end
