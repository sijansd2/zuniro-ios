//
//  AppDelegate.m
//  SymlexPro
//
//  Created by admin on 7/16/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "AppDelegate.h"
#import "AFNetworking.h"
#import "MyApp.h"
#import "CallViewController.h"
#import "Constans.h"
#import "NetworkManager.h"


#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [NetworkManager getInstance];
    [self initUserDefaultDatabase];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processIncomingCall:)
                                                 name: kSIPINCOMINGCall
                                               object:nil];
    
    
    //[self registerForRemoteNotification];
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error)
     {
         if( !error )
         {
             [[UIApplication sharedApplication] registerForRemoteNotifications];  // required to get the app to do anything at all about push notifications
             NSLog( @"Push registration success." );
         }
         else
         {
             NSLog( @"Push registration FAILED" );
             NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
             NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );
         }
     }];
    
    PKPushRegistry *pk = [[PKPushRegistry alloc] initWithQueue:dispatch_get_main_queue()];
    pk.delegate = self;
    pk.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP];
    
    
    return YES;
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    
     [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    
    NSUserDefaults* userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *userid = [userDefault objectForKey:data_user_name];
    NSString *token = @"888888899999999";
    
    if(userid != nil && token != nil){
        NSString *url_string = [insert_token_link stringByReplacingOccurrencesOfString:@"##userid##" withString:userid];
        url_string = [url_string stringByReplacingOccurrencesOfString:@"##token##" withString:token];
        [NSData dataWithContentsOfURL: [NSURL URLWithString:url_string]];
        
    }
    
    //[application unregisterForRemoteNotifications];
    NSLog(@"Application Terminated.......................");
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
-(void) initUserDefaultDatabase{
    
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity: 10];
    [self initUserDefaults:dict fromSettings:@"Advanced.plist"];
    [self initUserDefaults:dict fromSettings:@"Network.plist"];
    [self initUserDefaults:dict fromSettings:@"Phone.plist"];
    [self initUserDefaults:dict fromSettings:@"Codec.plist"];
    
    [userDef registerDefaults:dict];
    [userDef synchronize];
    
}


- (void)initUserDefaults:(NSMutableDictionary *)dict fromSettings:(NSString *)settings
{
    NSDictionary *prefItem;
    
    NSString *pathStr = [[NSBundle mainBundle] bundlePath];
    NSString *settingsBundlePath = [pathStr stringByAppendingPathComponent:@"Settings.bundle"];
    NSString *finalPath = [settingsBundlePath stringByAppendingPathComponent:settings];
    NSDictionary *settingsDict = [NSDictionary dictionaryWithContentsOfFile:finalPath];
    NSArray *prefSpecifierArray = [settingsDict objectForKey:@"PreferenceSpecifiers"];
    
    for (prefItem in prefSpecifierArray)
    {
        NSString *keyValueStr = [prefItem objectForKey:@"Key"];
        if (keyValueStr)
        {
            id defaultValue = [prefItem objectForKey:@"DefaultValue"];
            if (defaultValue)
            {
                [dict setObject:defaultValue forKey: keyValueStr];
            }
        }
    }
}


- (void)processIncomingCall:(NSNotification *)notification
{
    NSLog(@"INCOMING CALL.......................");
    int state = [[[ notification userInfo ] objectForKey: @"State"] intValue];
    
    if(state == PJSIP_INV_STATE_INCOMING)
    {
        
        [UIApplication sharedApplication].idleTimerDisabled=FALSE;
        
        NSString *phnNumber = [[ notification userInfo ] objectForKey: @"RemoteContact"];
        
        
        NSArray *arr =[phnNumber componentsSeparatedByString:@":"];
        NSArray *add = [arr[1] componentsSeparatedByString:@"@"];
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        CallViewController *callView = [mainStoryboard instantiateViewControllerWithIdentifier:@"callviewcontrollerId"];
        pjsua_call_id call_id =(pjsua_call_id)[[[ notification userInfo ] objectForKey:@"CallID"] intValue];
        NSString *callIDStr= [[ notification userInfo] objectForKey:@"callIdStr"];;
        [callView initWithCallId:call_id withString:callIDStr];
        callView.phoneNumber=add[0];
        callView.callType=callTypeIncoming;
        [self.window.rootViewController presentViewController:callView animated:YES completion:nil];
        
    }
    
    
}



#pragma mark - Remote Notification Delegate // <= iOS 9.x

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSString *strDevicetoken = [[NSString alloc]initWithFormat:@"%@",[[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""]];
    NSLog(@"Device Token = %@",strDevicetoken);
    self.strDeviceToken = strDevicetoken;
    
    NSUserDefaults* zemdata = [NSUserDefaults standardUserDefaults];
    [zemdata setObject:strDevicetoken forKey:data_device_token];

}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"Push Notification Information : %@",userInfo);
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"%@ = %@", NSStringFromSelector(_cmd), error);
    NSLog(@"Error = %@",error);
}

#pragma mark - UNUserNotificationCenter Delegate // >= iOS 10

//- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
//    
//    NSLog(@"User Info = %@",notification.request.content.userInfo);
//    
//    completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound);
//}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    
     [UIApplication sharedApplication].idleTimerDisabled=NO;
    NSLog(@"User Info = %@",response.notification.request.content.userInfo);
    
    completionHandler();
}

#pragma mark - Class Methods

/**
 Notification Registration
 */
- (void)registerForRemoteNotification {
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];
    }
    else {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}




-(void) pushRegistry:(PKPushRegistry *)registry didUpdatePushCredentials:(PKPushCredentials *)credentials forType:(PKPushType)type{
    
    NSString *strDevicetoken = [[NSString alloc]initWithFormat:@"%@",[[[credentials.token description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""]];
    
    NSLog(@"Token--%@",strDevicetoken);
    self.strDeviceToken = strDevicetoken;
    
    NSUserDefaults* zemdata = [NSUserDefaults standardUserDefaults];
    [zemdata setObject:strDevicetoken forKey:data_device_token];

}

-(void) pushRegistry:(PKPushRegistry *)registry didReceiveIncomingPushWithPayload:(PKPushPayload *)payload forType:(PKPushType)type{
    
    NSLog(@"push come...%@",payload.dictionaryPayload);
    
}



@end
