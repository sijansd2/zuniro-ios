//
//  CountryDataSource.h
//  SymlexPro
//
//  Created by USER on 8/17/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Country.h"

@interface CountryDataSource : NSObject
-(NSMutableArray *) getCountries;
@end
