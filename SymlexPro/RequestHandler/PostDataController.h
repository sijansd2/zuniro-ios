//
//  PostDataController.h
//  helloworld
//
//  Created by Riaz on 6/2/16.
//  Copyright © 2016 Riaz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol  PostDataControllerDelegate;

@interface PostDataController : NSObject


-(void) initWithURL:(NSString*) pageurl withData:(NSDictionary *)data;
-(void) uploadDataWithImage:(NSString*) pageurl withData:(NSDictionary *)data withImage:(UIImage*) image;
-(void) postwithURL:(NSString*) pageurl withData:(id)data withTag:(NSUInteger)tag;
-(void) uploadImageWithURL:(NSString*) pageurl withData:(NSDictionary *)data withImage:(UIImage*) image withTag:(NSUInteger) tag;

@property (weak, nonatomic) id <PostDataControllerDelegate>delegate;

@end

@protocol PostDataControllerDelegate<NSObject>
@optional
-(void) postFinishedWithData :(id)responseObject;
-(void) postFinishedWithError:(NSError *)error;
-(void) postFinishedWithData :(id)responseObject withTag:(NSUInteger)tag;

@end
