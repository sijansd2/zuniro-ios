//
//  CircluarBorderedButton.h
//  SymlexPro
//
//  Created by USER on 8/13/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface CircluarBorderedButton : UIButton

@property(nonatomic, strong) IBInspectable UIColor *borderColor;
@end
