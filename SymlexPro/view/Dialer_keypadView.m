//
//  Dialer_keypadView.m
//  ipjsua
//
//  Created by Riaz Hasan on 9/19/15.
//  Copyright (c) 2015 Teluu. All rights reserved.
//

#import "Dialer_keypadView.h"
#import <AudioToolbox/AudioToolbox.h>

@interface Dialer_keypadView (){
    
    //contains numerical digits
   // NSArray *digits;
    //contains images for numerical digits
    NSArray *digits_number;
}

@end
@implementation Dialer_keypadView
@synthesize delegate;

-(void) viewDidLoad{
    [super viewDidLoad];
    
    
    //assign images in the digits
   // digits = [NSArray arrayWithObjects:@"dial_num_1_wht.png", @"dial_num_2_wht.png", @"dial_num_3_wht.png", @"dial_num_4_wht.png",@"dial_num_5_wht.png", @"dial_num_6_wht.png", @"dial_num_7_wht.png",@"dial_num_8_wht.png",@"dial_num_9_wht.png",@"dial_num_star_wht.png",@"dial_num_0_wht.png",@"dial_num_pound_wht.png", nil];
    
    //assign digits
    digits_number=[NSArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"*",@"0",@"#",nil];
    
   // self.view.backgroundColor = [PjsuaUtils colorWithHexString:textColor];
}





- (IBAction)myClickEvent:(UIButton *)sender {
    
   
    
    
    //NSLog(@"%ld",(long)indexPath.row);
    
    //send the pressed number to phoneViewController
      [self sendToPhoneView:sender.titleLabel.text];
}


-(void) sendToPhoneView:(NSString*)number{

    //play sound for the pressed key
    [self playSoundForKey:number];
    
    //send data via protocol we defined
    if(self.delegate != nil){
        if ([self.delegate respondsToSelector:@selector(keypadPressedWithValue:)]) {
            [self.delegate keypadPressedWithValue:number];
        }
    }

}


- (void)playSoundForKey:(NSString*)key
{
    
    //get the pound key tone
    if([key  isEqual:@"*"]){
    
        key=@"s";
        
    }
    
        NSBundle *mainBundle = [NSBundle mainBundle];
        NSString *filename = [NSString stringWithFormat:@"dtmf-%@",key];
    
        //get file path
        NSString *path = [mainBundle pathForResource:filename ofType:@"aif"];
        if (!path)
            return;
        //convert it to the url
        NSURL *aFileURL = [NSURL fileURLWithPath:path isDirectory:NO];
        if (aFileURL != nil)
        {
            SystemSoundID aSoundID;
            OSStatus error = AudioServicesCreateSystemSoundID((__bridge CFURLRef)aFileURL,
                                                              &aSoundID);
            if (error != kAudioServicesNoError)
                return;
            //play using avaudioplayer
            AudioServicesPlaySystemSound(aSoundID);
        }
    
    
    
}



@end
