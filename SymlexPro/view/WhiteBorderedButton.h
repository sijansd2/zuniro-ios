//
//  WhiteBorderedButton.h
//  SymlexPro
//
//  Created by USER on 8/10/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface WhiteBorderedButton : UIButton

@property(nonatomic) IBInspectable BOOL isBtnTypeCancel;

@property(nonatomic, strong) IBInspectable UIColor *borderColor;

@property(nonatomic, strong) IBInspectable UIColor *backgroundNormalColor;

@property(nonatomic, strong) IBInspectable UIColor *customTextColor;

@end
