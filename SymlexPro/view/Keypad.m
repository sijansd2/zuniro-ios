//
//  Keypad.m
//  SymlexPro
//
//  Created by USER on 8/16/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "Keypad.h"

@implementation Keypad

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self addAction];
    }
    return self;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self addAction];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addAction];
    }
    return self;
}

- (void)addAction {
    [self addTarget:self action:@selector(pressed:) forControlEvents:UIControlEventTouchDown];
    [self addTarget:self action:@selector(pressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addTarget:self action:@selector(pressed:) forControlEvents:UIControlEventTouchUpOutside];
    [self addTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventTouchCancel];
    [self addTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventPrimaryActionTriggered];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.adjustsImageWhenHighlighted = NO;
    self.backgroundColor=[UIColor redColor];
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName:[UIColor redColor],
                              NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:12]
                              };
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:@"riaz" attributes:attribs];

    self.titleLabel.attributedText= attributedText;
    
}

- (void)pressed:(UIButton *)btn {
    [btn setBackgroundColor:[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1]];
}

- (void)touchUp:(UIButton *)btn {
    [btn setBackgroundColor:[UIColor whiteColor]];
}
@end
