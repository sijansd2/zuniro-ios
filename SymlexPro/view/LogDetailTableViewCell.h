//
//  LogDetailTableViewCell.h
//  SymlexPro
//
//  Created by USER on 9/18/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogDetailTableViewCell : UITableViewCell

@property(weak,nonatomic) IBOutlet UILabel *lbTime;
@property(weak,nonatomic) IBOutlet UILabel *lbCallStatus;
@property(weak,nonatomic) IBOutlet UIImageView *callPhoto;

@end
