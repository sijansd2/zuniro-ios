//
//  SetPinView.h
//  SymlexPro
//
//  Created by USER on 8/19/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostDataController.h"
@interface SetPinView : UIView<PostDataControllerDelegate>

@property(nonatomic,weak) IBOutlet UITextField *tfPincode;
@property(nonatomic,weak) IBOutlet UITextField *tfConfirmPincode;

@property(nonatomic,weak) IBOutlet UIButton *btnSetPin;
@property(nonatomic,weak) IBOutlet UIButton *btnClear;


-(IBAction) btnSetPressed:(id)sender;
-(IBAction) btnClearPressed:(id)sender;

@end
