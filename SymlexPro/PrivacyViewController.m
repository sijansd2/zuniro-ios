//
//  PrivacyViewController.m
//  SymlexPro
//
//  Created by admin on 9/18/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "PrivacyViewController.h"
#import "SVProgressHUD.h"
#import "UIView+Toast.h"
#import "Constans.h"

@interface PrivacyViewController ()

@end

@implementation PrivacyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    
    BOOL network = [NetworkManager getNetworkStatus];
    
    if(network){
       // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            //Your code to execute in background...
            [self loadWebView];
       // });
    }
    else{
        [self.view makeToast:@"Please Check Your Internet"];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(netStat:)
                                                 name: NetworkUpdate
                                               object:nil];
}

-(void)loadWebView{
    
    NSString *urlString = @"http://www.symlex.com/privacy_policy/";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [self.webPrivacy loadRequest:urlRequest];
    self.webPrivacy.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    [SVProgressHUD showWithStatus:@"Please Wait.."];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [SVProgressHUD dismiss];
}

-(void)netStat:(NSNotification *)notification{
    
    
    int state = [[[ notification userInfo ] objectForKey: @"netStat"] intValue];
    
    if(state==1){
       [self loadWebView];
        
    }
    
}


@end
