//
//  PrivacyViewController.h
//  SymlexPro
//
//  Created by admin on 9/18/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkManager.h"

@interface PrivacyViewController : UIViewController<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webPrivacy;

@end
