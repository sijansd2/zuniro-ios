//
//  RootViewController.m
//  SymlexPro
//
//  Created by admin on 7/26/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "MyRootViewController.h"
#import "MyApp.h"
#import "Constans.h"
#import "AppDelegate.h"
#import "SignInViewController.h"
#import "REFrostedViewController.h"
#import "SideViewController.h"

@interface MyRootViewController ()

@end

@implementation MyRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults* zemData = [NSUserDefaults standardUserDefaults];
    NSString* user = [zemData objectForKey:data_sip_ip];
    if(user == nil){
        [self signinViewOpen];
    }else{
        [self tabViewOpen];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)signinViewOpen{
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SignInViewController *tc =[self.storyboard instantiateViewControllerWithIdentifier:@"signinView"];
    [app.window setRootViewController:tc];
}

-(void) tabViewOpen{
    UITabBarController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"tabview"];
    SideViewController *menuController = [self.storyboard instantiateViewControllerWithIdentifier:@"sideview"];
    REFrostedViewController *frostedViewController = [[REFrostedViewController alloc] initWithContentViewController:navigationController menuViewController:menuController];
    frostedViewController.direction = REFrostedViewControllerDirectionLeft;
    
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app.window setRootViewController:frostedViewController];
}


@end
