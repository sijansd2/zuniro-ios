//
//  OtpViewController.m
//  SymlexPro
//
//  Created by USER on 8/19/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "OtpViewController.h"
#import "Constans.h"
#import "SignInViewController.h"
#import "UIView+Toast.h"
#import "MyEncryption.h"
#import "PINViewController.h"
#import "TabViewController.h"
#import "NetworkManager.h"
@interface OtpViewController (){

    NSUserDefaults *userDefault;
}

@end

@implementation OtpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefault = [NSUserDefaults standardUserDefaults];
    [self.tfPincode addTarget:self action:@selector(updateSubmitButton) forControlEvents:UIControlEventEditingChanged];
    [self updateSubmitButton];
    self.wellcomeView.hidden = true;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateSubmitButton {
    BOOL textFieldsNonEmpty = self.tfPincode.text.length > 0;
    self.btnSetPin.enabled= textFieldsNonEmpty;
}

-(IBAction) btnSetPinPressed:(id)sender{

    Boolean network = [NetworkManager getNetworkStatus];
    
    if(network){
    
    if(self.tfPincode.text.length>0){
        
        if(self.userID == nil && [self.userExist isEqualToString:@"User exist. Confirmation code sent again"]){
            
            [userDefault setObject:self.tfPincode.text forKey:data_password];
            NSString *username = [userDefault objectForKey:data_user_name];
            [self check_user:username withPassword:self.tfPincode.text];
            
            return;
        }
        
        
        NSString *url= [NSString stringWithFormat: @"%@%@",baseUrl,verifyOtp];;
        
        PostDataController *controller = [[PostDataController alloc] init];
        controller.delegate = self;
        
        
        NSDictionary *param = @{@"user_id": self.userID,
                                    @"api_key": apiKey,
                                    @"otp": self.tfPincode.text
                                    };
        [controller postwithURL:url withData:param withTag:103];
        }
        
    }
    else{
        [self.view makeToast:@"Please Check Your Internet"];
    }
}

- (IBAction)proceedToLogin:(id)sender {
    
    SignInViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginview"];
    [self.navigationController pushViewController:viewController animated:YES];
}


-(void) check_user:(NSString *) userName withPassword:(NSString *) password{
    
    PostDataController *controller = [[PostDataController alloc] init];
    controller.delegate = self;
    NSString *url= [NSString stringWithFormat: @"%@%@",baseUrl,check_user];
    
    NSString* md5Str = [[NSString stringWithFormat:@"%@%@",userName,password] MD5String];
    
    NSDictionary *param = @{@"username": userName,
                            @"password": password,
                            @"sec_key" : md5Str,
                            @"api_key" :apiKey
                            };

    
    [controller postwithURL:url withData:param withTag:100];
    
}

-(void) postFinishedWithData:(id)responseObject withTag:(NSUInteger)tag{

    if(tag ==100){
        //check user
        if(responseObject != nil){
            if([responseObject[@"message"] isEqualToString:@"Sip user available"]){
                [self checkPin];
            }else{
            
                [self.view makeToast:@"User Not Found"];
                
            }
        }
    }else if(tag == 101){
        //check pin
        if(responseObject != nil){
        
            NSString* msg = [responseObject objectForKey:@"message"];
            if(![msg isEqualToString:@"Pin already Set"]){
                [userDefault setObject:self.tfPincode.text forKey:data_password];
                PINViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"pinviewC"];
                viewController.isFirstTime = YES;
                [self.navigationController pushViewController:viewController animated:YES];
                
            }else{
                
                [userDefault setObject:@"true" forKey:data_isAccountCreated];
                TabViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tabview"];
                [self.navigationController pushViewController:viewController animated:YES];
                //user is set to go
                //go to tabview controller
            }
        }
    }else if(tag == 103){
        if(responseObject != nil){
        //verify otp
            if([responseObject[@"message"] isEqualToString:@"success"]){
                
                 [userDefault setObject:self.tfPincode.text forKey:data_password];
                PINViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"pinviewC"];
                viewController.isFirstTime = YES;
                [self.navigationController pushViewController:viewController animated:YES];
                
            }else{
                [self.view makeToast:responseObject[@"message"]];
            }
        }
    }
    
}

-(void)checkPin{
    NSString *username = [userDefault objectForKey:data_user_name];
    NSString *password = [userDefault objectForKey:data_password];
    
    NSString* url = [NSString stringWithFormat: @"%@%@",baseUrl,checkPin];
    NSString* md5Str = [[NSString stringWithFormat:@"%@%@",username,password] MD5String];
    NSDictionary *param = @{@"username": username,
                            @"password": password,
                            @"api_key": apiKey,
                            @"sec_key": md5Str};
    
    PostDataController *controller = [[PostDataController alloc] init];
    controller.delegate = self;
    [controller postwithURL:url withData:param withTag:101];
    
}

-(void) postFinishedWithError:(NSError *)error{

    [self.view makeToast:[error.userInfo valueForKey:@"NSLocalizedDescription"]];
    
}

-(void) gotoWellcomeView{

    [UIView transitionWithView:self.container
                      duration:.5
                       options:UIViewAnimationOptionTransitionFlipFromRight
                    animations: ^{
                        
                            self.otpView.hidden = true;
                            self.wellcomeView.hidden = false;
                        
                    }
     
                    completion:^(BOOL finished) {
                        if (finished) {
                            
                        }
                    }];
}

@end
