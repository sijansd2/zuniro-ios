//
//  SignUpViewController.m
//  SymlexPro
//
//  Created by admin on 7/18/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "SignUpViewController.h"
#import "ViewExtension.h"
#import "TextFieldUtils.h"
#import "CountryDataSource.h"
#import "UIViewController+MJPopupViewController.h"
#import "LoginManager.h"
#import "Constans.h"
#import "MyEncryption.h"
#import "OtpViewController.h"
#import "UIView+Toast.h"
#import "MainPageViewController.h"
#import "SVProgressHUD.h"
#import "Base64.h"
#import "TabViewController.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "NetworkManager.h"

@interface SignUpViewController (){

    NSString *countryCode;
    NSUserDefaults *userDefault;
    
}

@property (strong, nonatomic) NSArray *countryObjects;

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefault =[NSUserDefaults standardUserDefaults];
    
    // Do any additional setup after loading the view.
    [self setBackGroundImage:@"sign_in_background.jpg"];
    [self updateView];
}

-(void) viewWillAppear:(BOOL)animated{

    
    
}

- (IBAction)btnBackPressed:(id)sender {
    
    MainPageViewController *viewController = [self.navigationController.viewControllers objectAtIndex:0];
    [self.navigationController popToViewController:viewController animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL) textFieldShouldBeginEditing:(UITextField *)textField{
    
    if(textField == self.tfCountryName){
        
        [self.view endEditing:YES];
        ComboView *popupview = [self.storyboard instantiateViewControllerWithIdentifier:@"comboview"];
        popupview.delegate = self;
        popupview.comboType = @"Country";
        popupview.comboNameProperty = @"name";
        popupview.comboArray=[self allCountryObjects];
        [self presentPopupViewController:popupview animationType:MJPopupViewAnimationFade];
        return NO;
    }
    
    return YES;
    
}

-(void) updateView{
    
    self.tfCountryName.delegate=self;
    [TextFieldUtils addImageLeftSideTextView:self.tfCountryName imageName:@"flag.png"];
    [TextFieldUtils addImageRightSideTextView:self.tfCountryName imageName:@"arrow_right.png"];
    [TextFieldUtils addImageLeftSideTextView:self.tfNumber imageName:@"phone.png"];
    [TextFieldUtils addImageLeftSideTextView:self.tfPassword imageName:@"password.png"];
    [TextFieldUtils addImageLeftSideTextView:self.tfConfirmPassword imageName:@"password.png"];
    [TextFieldUtils addImageLeftSideTextView:self.tfEmail imageName:@"mail.png"];
    
    self.tfCountryName.textColor = [UIColor whiteColor];
    self.tfPassword.textColor = [UIColor whiteColor];
    self.tfNumber.textColor = [UIColor whiteColor];
    self.tfConfirmPassword.textColor = [UIColor whiteColor];
    self.tfEmail.textColor = [UIColor whiteColor];
    
    [self.tfCountryName addTarget:self action:@selector(updateLogInButton) forControlEvents:UIControlEventEditingChanged];
    [self.tfNumber addTarget:self action:@selector(updateLogInButton) forControlEvents:UIControlEventEditingChanged];
    [self.tfPassword addTarget:self action:@selector(updateLogInButton) forControlEvents:UIControlEventEditingChanged];
    [self.tfConfirmPassword addTarget:self action:@selector(updateLogInButton) forControlEvents:UIControlEventEditingChanged];
    [self.tfEmail addTarget:self action:@selector(updateLogInButton) forControlEvents:UIControlEventEditingChanged];
    [self updateLogInButton];
}


- (NSArray *)allCountryObjects
{
    if(!self.countryObjects){
        CountryDataSource *c = [[CountryDataSource alloc] init];
        NSMutableArray *countryNames = [c getCountries];
        [self setCountryObjects:[NSArray arrayWithArray:countryNames]];
    }
    
    return self.countryObjects;
}

-(void)userFinishedChoosingitem:(NSDictionary *)item andType:(NSString *)type{
    
    
    self.tfCountryName.text = item[@"name"];
    countryCode = item[@"code"];
    
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    CGSize stringBoundingBox = [countryCode sizeWithFont:font];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 35+stringBoundingBox.width, 40)];
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(10, 8, 24, 24)];
    imageview.image = [UIImage imageNamed:@"phone.png"];
    imageview.contentMode = UIViewContentModeScaleAspectFit;
    self.tfNumber.leftViewMode = UITextFieldViewModeAlways;
    [view addSubview:imageview];
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(35, -1, stringBoundingBox.width, 39)];
    label.text = countryCode;
    label.textColor=[UIColor whiteColor];
    label.font = font;
    [view addSubview:label];
    self.tfNumber.leftViewMode = UITextFieldViewModeAlways;
    self.tfNumber.leftView = view;
    [self.tfNumber clipsToBounds];
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
}

-(void) closeButtonClicked{
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
}

- (IBAction)btnSignUpPressed:(id)sender {
    [self.view endEditing:YES];
    
    Boolean network = [NetworkManager getNetworkStatus];
    if(network){
        GetDataController *controller = [[GetDataController alloc] init];
            controller.delegate = self;
            [controller getFromUrl:configURL withData:nil withTag:100];

    }
    else{
        [self.view makeToast:@"Please Check Your Internet"];
    }
    

}



-(void) getFinishedWithData:(id)responseObject withTag:(NSUInteger)tag{

    NSLog(@"%@",responseObject);
    if(tag == 100){
        
        if (![responseObject isEqualToString:@"99"])
        {
            NSData	*b64DecData = [Base64 decode:responseObject];
            NSString *msg = [[NSString alloc] initWithData:b64DecData encoding:NSASCIIStringEncoding];
            XMLParser *xmlParser =[[XMLParser alloc] init];
            xmlParser.delegate = self;
            [xmlParser parseData:msg];
        }
    }
    
}

-(void) getFinishedWithError:(NSError *)error{

    NSLog(@"%@",error);
}

-(void) xmlParserFinishedParsingConfig{

    NSUserDefaults *newUser = [NSUserDefaults standardUserDefaults];

    NSString *sipIP = [newUser objectForKey:data_sip_ip];
    
    if(sipIP != nil && ![sipIP isEqualToString:@""]){
    
        [self signUP];
    }

}


-(void) signUP{
    
    NSString* url = [NSString stringWithFormat: @"%@%@",baseUrl,signUp];
    
    NSString *phoneNumberWithCCode= [self phoneNumberWithCCode:countryCode withNumber:self.tfNumber.text];
    
    NSLog(@"----%@",phoneNumberWithCCode);
    
    [userDefault setObject:phoneNumberWithCCode forKey:data_user_name];
    
    NSDictionary *param = @{
                            @"ccd": countryCode,
                            @"api_key": apiKey,
                            @"phone": self.tfNumber.text,
                            };
    
    PostDataController *controller = [[PostDataController alloc] init];
    controller.delegate = self;
    [controller initWithURL:url withData:param];
}


-(void) postFinishedWithData:(id)responseObject{
    
    NSLog(@"%@",responseObject);
    
    if(responseObject != nil){
    
        if([responseObject[@"message"] isEqualToString:@"Please verify your number. Confirmation code sent"]){
            OtpViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"otpView"];
            viewController.userID =  responseObject[@"user_id"];
            viewController.token = responseObject[@"token"];
            [self.navigationController pushViewController:viewController animated:YES];
            
        }else if([responseObject[@"message"] isEqualToString:@"User exist. Confirmation code sent again"]){
            OtpViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"otpView"];
            viewController.userExist =  responseObject[@"message"];
            [self.navigationController pushViewController:viewController animated:YES];
            
        }else{
        
            [self showError:responseObject[@"message"]];
        
        }
    }else{
    
        [self showError:@"Operation failed"];
    }
    
    
}

-(void) postFinishedWithError:(NSError *)error{

    NSLog(@"%@",error);
    
}

-(void) showError:(NSString *) error{
    [self.view makeToast:error];
}

-(NSString *) phoneNumberWithCCode:(NSString *) code withNumber:(NSString *) phoneNumber{
    return [self removeUnnecssaryCharacter:[NSString stringWithFormat:@"%@%@",code,phoneNumber]];;
}

- (NSString *)removeUnnecssaryCharacter:(NSString *)string {
    
    NSString * strippedNumber = [string stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [string length])];
    
    return strippedNumber;
}

- (void)updateLogInButton {
    BOOL textFieldsNonEmpty = self.tfCountryName.text.length > 0 && self.tfNumber.text.length>6;
  //  BOOL isValidEmail = [TextFieldUtils NSStringIsValidEmail:self.tfEmail.text];
    self.btnSignUp.enabled = textFieldsNonEmpty;
}


@end
