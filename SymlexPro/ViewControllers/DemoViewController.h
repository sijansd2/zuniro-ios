//
//  DemoViewController.h
//  SymlexPro
//
//  Created by admin on 7/26/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DemoViewController : UIViewController

- (IBAction)btnPressed:(id)sender;
@end
