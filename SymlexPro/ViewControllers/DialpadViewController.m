//
//  DialpadViewController.m
//  SymlexPro
//
//  Created by admin on 7/26/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "DialpadViewController.h"
#import "Constans.h"
#import <AudioToolbox/AudioToolbox.h>
#import "UIView+Toast.h"
#import "AFNetworking.h"
#import "CallViewController.h"
#import "MyApp.h"
#import "NSNotificationAdditions.h"

@interface DialpadViewController (){
    NSUserDefaults* userDefault;
}

@end
Dialer_keypadView *dialerKeyPad;

@implementation DialpadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefault = [NSUserDefaults standardUserDefaults];
    
    [self setNeedsStatusBarAppearanceUpdate];
    if(self.phoneNumber){
    
        self.tfDialpad.text = self.phoneNumber;
    }
    //focus the dialpad text so the cursor is always visible
    self.tfDialpad.delegate=self;
    
    //for hiding keyboard we use a dummyview view as input view of dialpad
    UIView* dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    self.tfDialpad.inputView = dummyView;
    [self.tfDialpad becomeFirstResponder];
    self.lbRegStatus.text=@"Trying To Register";

    //assign long press in delete button
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(DeletelongPress:)];
    [self.btnDelete addGestureRecognizer:longPress];
   
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(netStat:)
                                                 name: NetworkUpdate
                                               object:nil];
}

-(void) viewWillAppear:(BOOL)animated{

    [self.tfDialpad becomeFirstResponder];
    [self balanceUpdate];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) keypadPressedWithValue:(NSString *)value{
    
    //get cursor position
    UITextRange *cursorPosition = self.tfDialpad.selectedTextRange;
    //get the cursor exact location in int
    NSInteger endOffset = [self.tfDialpad offsetFromPosition:self.tfDialpad.beginningOfDocument toPosition:cursorPosition.end];
    
    
    //comp dialpadtext
    NSMutableString * temp = [[NSMutableString alloc] initWithString:self.tfDialpad.text];
    
    //insert the new data in current cursor position
    [temp insertString:value atIndex:endOffset];
    //temp = [temp stringByAppendingString:value];
    
    //update dialpadText with new data
    self.tfDialpad.text=temp;
    
    //set cursor postion for next data
    UITextPosition *nextposition = [self.tfDialpad positionFromPosition:cursorPosition.end offset:1];
    
    [self.tfDialpad setSelectedTextRange:[self.tfDialpad textRangeFromPosition:nextposition
                                                              toPosition:nextposition]];
    
    
    
    //[self changeCallBtnColler];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

// It is important for you to hide the keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
-(void) DeletelongPress:(UILongPressGestureRecognizer*)gesture{
    
    //emty dialpadText text in long press
    self.tfDialpad.text=@"";
    
}


- (IBAction)deletePresses:(id)sender {
    
    //get current cursor position
    UITextRange *cursorPosition = self.tfDialpad.selectedTextRange;
    
    NSInteger endOffset = [self.tfDialpad offsetFromPosition:self.tfDialpad.beginningOfDocument toPosition:cursorPosition.end];
    
    
    NSString *str = self.tfDialpad.text;
    if(str.length !=0 && endOffset>0){
        //remove the current charcter in current cursor position
        str = [str stringByReplacingCharactersInRange:NSMakeRange(endOffset-1, 1) withString:@""];
        
        //NSString *truncatedString = [str substringToIndex:[str length]-1];
        
        //update dialpadText
        self.tfDialpad.text = str;
        
        //update cursor position
        UITextPosition *nextposition = [self.tfDialpad positionFromPosition:cursorPosition.end offset:-1];
        
        [self.tfDialpad setSelectedTextRange:[self.tfDialpad textRangeFromPosition:nextposition
                                                                  toPosition:nextposition]];
        
        
    }
}

-(IBAction) btnCancelPressed:(UIButton *)sender{

    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction) keyEvent:(UIButton *)sender{

    if(sender.tag ==10){
        [self sendToPhoneView:@"*"];
    }else if(sender.tag == 11){
        [self sendToPhoneView:@"#"];
    }else{
        [self sendToPhoneView:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
    }
    
}

-(void) sendToPhoneView:(NSString*)number{
    
    //play sound for the pressed key
    [self playSoundForKey:number];
    
    [self keypadPressedWithValue:number];
 
}
- (void)playSoundForKey:(NSString*)key
{
    
    //get the pound key tone
    if([key  isEqual:@"*"]){
        
        key=@"s";
        
    }
    
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *filename = [NSString stringWithFormat:@"dtmf-%@",key];
    
    //get file path
    NSString *path = [mainBundle pathForResource:filename ofType:@"aif"];
    if (!path)
        return;
    //convert it to the url
    NSURL *aFileURL = [NSURL fileURLWithPath:path isDirectory:NO];
    if (aFileURL != nil)
    {
        SystemSoundID aSoundID;
        OSStatus error = AudioServicesCreateSystemSoundID((__bridge CFURLRef)aFileURL,
                                                          &aSoundID);
        if (error != kAudioServicesNoError)
            return;
        //play using avaudioplayer
        AudioServicesPlaySystemSound(aSoundID);
    }
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)balanceUpdate{
    
    
    Boolean network = [NetworkManager getNetworkStatus];
    if(network){
        
        NSString* user = [userDefault stringForKey:data_user_name];
        NSString* url = [NSString stringWithFormat: @"%@%@%@/%@",baseUrl,balance,user,apiKey];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"%@",responseObject);
            NSDictionary *jsonOutput = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            
            if([jsonOutput[@"message"] isEqualToString:@"success"]){
                self.lblBalance.text = [NSString stringWithFormat:@"%@",jsonOutput[@"balance"]];
                NSLog(@"%@",jsonOutput[@"balance"]);
            }
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            NSLog(@"%@",error);
            
        }];
    }
    else{
        [self.view makeToast:@"Please Check Your Internet"];
    }
    
    

}


-(void) getFinishedWithData:(id)responseObject withTag:(NSUInteger)tag{
    
    NSLog(@"%@",responseObject);
    if(tag == 100){
        
        if (![responseObject isEqualToString:@"99"])
        {
           NSLog(@"%@",responseObject[@"message"]);
        }
    }
    
}

-(void) getFinishedWithError:(NSError *)error{
    
    NSLog(@"%@",error);
}


-(void)netStat:(NSNotification *)notification{
    
    
    NSLog(@"%@",[ notification userInfo ]);
    
    int state = [[[ notification userInfo ] objectForKey: @"netStat"] intValue];
    
    if(state==1){
        [self balanceUpdate];
    
    }
    
}



- (IBAction)makeCallPressed:(id)sender {
    
    NSString *number = self.tfDialpad.text;
    pjsua_acc_id acc_id = 0;
    pjsua_acc_info infos;
    
    pjsua_acc_get_info(acc_id,&infos);
    //check if number is available in dialpadtext
    if(number.length >0){
        //check current registration status
        if(infos.status==200 && infos.expires>0){
            
            CallViewController *callView = [self.storyboard instantiateViewControllerWithIdentifier:@"callviewcontrollerId"];
            callView.phoneNumber=number;
            callView.callType=callTypeDialed;
            [self presentViewController:callView animated:YES completion:nil];
            
        }
    }else{
        
        NSLog(@"No number pressed");
        
    }
}

- (IBAction)addPressed:(id)sender {
    
        [self createNewPerson:self.tfDialpad.text];
}

- (void)createNewPerson:(NSString*)number
{
    
    ABRecordRef newPerson = ABPersonCreate();
    CFErrorRef error = NULL;
    
    ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(phoneNumberMultiValue,(__bridge CFTypeRef)(number), (CFStringRef)@"home", NULL);
    ABRecordSetValue(newPerson, kABPersonPhoneProperty, phoneNumberMultiValue, nil);
    NSAssert( !error, @"Something bad happened here." );
    
    // Create and set-up the new person view controller
    ABNewPersonViewController* newPersonViewController = [[ABNewPersonViewController alloc] initWithNibName:nil bundle:nil];
    [newPersonViewController setDisplayedPerson:newPerson];
    [newPersonViewController setNewPersonViewDelegate:self];
    
    // Wrap in a nav controller and display
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:newPersonViewController];
    [self presentModalViewController:navController animated:YES];
    
    CFRelease(newPerson);
}

-(void)newPersonViewController:(ABNewPersonViewController *)newPersonView didCompleteWithNewPerson:(ABRecordRef)person{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
