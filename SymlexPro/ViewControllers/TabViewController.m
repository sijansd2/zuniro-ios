//
//  TabViewController.m
//  SymlexPro
//
//  Created by admin on 7/26/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "TabViewController.h"
#import "ColorUtility.h"
#import "MyApp.h"
#import "AppDelegate.h"
#import "Constans.h"
#import "AFNetworking.h"

@interface TabViewController ()

@end

@implementation TabViewController
NSString *remoteHostName = @"www.google.com";
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NetworkManager *manager = [NetworkManager getInstance];
    // Do any additional setup after loading the view.
    
    UITabBarItem *item1 = [self.tabBar.items objectAtIndex:0];
    UITabBarItem *item2 = [self.tabBar.items objectAtIndex:1];
    UITabBarItem *item3 = [self.tabBar.items objectAtIndex:2];
    UITabBarItem *item4 = [self.tabBar.items objectAtIndex:3];
    
    
    //adding tab images in tab bar controller
    item1.image = [[UIImage imageNamed:@"contacts"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item1.selectedImage = [[UIImage imageNamed:@"contact_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    item2.image = [[UIImage imageNamed:@"calls"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item2.selectedImage = [[UIImage imageNamed:@"call_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    item3.image = [[UIImage imageNamed:@"accounts"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item3.selectedImage = [[UIImage imageNamed:@"account_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    item4.image = [[UIImage imageNamed:@"settings"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item4.selectedImage = [[UIImage imageNamed:@"settings_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor darkGrayColor] }
                                             forState:UIControlStateNormal];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [ColorUtility colorWithHexString:textColor] }
                                            forState:UIControlStateSelected];
    
    //[[self tabBar] setTintColor:[UIColor darkGrayColor]];
    [[UITabBar appearance] setBarTintColor:[UIColor whiteColor]];
    
    [self getDataFromApi];

}


-(void) getDataFromApi{

    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    NSString *url = @"http://conf.aussdjf.xyz/iosapi/zuniro/ios_api.html";
    
    
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"%@",responseObject);
        NSString *aStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        if([aStr containsString:@"\n"]){
            aStr = [aStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        }
        
        if([aStr containsString:@"\r"]){
            aStr = [aStr stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        }
        
        NSString * appBuildNum = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
        NSLog(@"appBuildNum-------------%@----%@",appBuildNum,aStr);
        if (aStr != nil && [aStr isEqualToString:appBuildNum])
        {
            NSUserDefaults *userDef= [NSUserDefaults standardUserDefaults];
            [userDef setObject:@"true" forKey:@"isIpv6"];
            
        }else{
        
            NSUserDefaults *userDef= [NSUserDefaults standardUserDefaults];
            [userDef setObject:@"false" forKey:@"isIpv6"];
            
        }
        
        restartPJSIP();
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(netStat:)
                                                     name: NetworkUpdate
                                                   object:nil];
        
        [self configPushsService];

        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"%@",error);
        
        restartPJSIP();
        NSUserDefaults *userDef= [NSUserDefaults standardUserDefaults];
        [userDef setObject:@"false" forKey:@"isIpv6"];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(netStat:)
                                                     name: NetworkUpdate
                                                   object:nil];
        
        [self configPushsService];
        
    }];
    

}

-(void)configPushsService{
    
    NSUserDefaults* userDefault = [NSUserDefaults standardUserDefaults];

    NSString *userid = [userDefault objectForKey:data_user_name];
    NSString *token = [userDefault objectForKey:data_device_token];
    
    if(userid != nil && token != nil){
        NSString *url_string = [insert_token_link stringByReplacingOccurrencesOfString:@"##userid##" withString:userid];
        url_string = [url_string stringByReplacingOccurrencesOfString:@"##token##" withString:token];
        [NSData dataWithContentsOfURL: [NSURL URLWithString:url_string]];
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)netStat:(NSNotification *)notification{
    NSLog(@"-----))))(((((-------");
    sip_set_registration();
}

@end
