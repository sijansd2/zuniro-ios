//
//  ContactsViewController.h
//  SymlexPro
//
//  Created by admin on 7/31/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>

@interface ContactsViewController : UIViewController<UISearchBarDelegate ,UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *contactsTable;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, assign) bool isFiltered;

-(IBAction) btnDialpadPressed:(id)sender;

@end
