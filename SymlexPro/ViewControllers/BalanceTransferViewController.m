
#import "BalanceTransferViewController.h"
#import "Constans.h"
#import "MyEncryption.h"
#import "NetworkManager.h"
#import "UIView+Toast.h"

@interface BalanceTransferViewController (){
    
    NSUserDefaults* userDefault;
}

@end

@implementation BalanceTransferViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    userDefault = [NSUserDefaults standardUserDefaults];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self updateView];
    
    //Tap gesture enable
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiskeyboard)];
    [self.view addGestureRecognizer:tap];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



//MARK: Http post callbacks
-(void)postFinishedWithData:(id)responseObject{
    NSString *msg = [responseObject objectForKey:@"message"];
    [self showAlert:@"Server Response" messageBody:msg];
    
}

-(void)postFinishedWithData:(id)responseObject withTag:(NSUInteger)tag{
    NSLog(@"Server response : postFinishedWithData");
    
}


-(void)postFinishedWithError:(NSError *)error{
    NSLog(@"%@",error);
    [self showAlert:@"Error" messageBody:[error.userInfo valueForKey:@"NSLocalizedDescription"]];
    
}


//MARK: Alert
-(void) showAlert:(NSString*)title messageBody:(NSString*)msg{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Server Response" message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Ok"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    //NSLog(@"you pressed Yes button");
                                    
                                }];
    
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}

//MARK: Pressed functions
- (IBAction)transferPressed:(id)sender {

    Boolean network = [NetworkManager getNetworkStatus];
    if(network){
        [self balanceTransfer];
        [self dismiskeyboard];
    }
    else{
        [self.view makeToast:@"Please Check Your Internet"];
    }
}

- (IBAction)cancelPressed:(id)sender {
    [self dismiskeyboard];
    [self dismissViewControllerAnimated:YES completion:nil];
}


//MARK: Functions
-(void)balanceTransfer{
    NSString* user = [userDefault stringForKey:data_user_name];
    NSString* pass = [userDefault stringForKey:data_password];
    NSString* url = [NSString stringWithFormat: @"%@%@",baseUrl,balanceTransfer];
    NSString* md5Str = [[NSString stringWithFormat:@"%@%@",user,pass] MD5String];
    NSDictionary *param = @{@"username": user,
                            @"password": pass,
                            @"api_key": apiKey,
                            @"sec_key": md5Str,
                            @"to_pin": self.tfToNumber.text,
                            @"amount": self.tfAmount.text,
                            @"secret_pin": self.tfSecretPin.text};
    
    PostDataController *controller = [[PostDataController alloc] init];
    controller.delegate = self;
    [controller initWithURL:url withData:param];
}


-(void)dismiskeyboard{
    [self.tfToNumber resignFirstResponder];
    [self.tfAmount resignFirstResponder];
    [self.tfSecretPin resignFirstResponder];
}

-(void)updateView{
    self.btnTransfer.enabled = false;
    [self.tfToNumber addTarget:self action:@selector(updateTransferButton) forControlEvents:UIControlEventEditingChanged];
    
    [self.tfAmount addTarget:self action:@selector(updateTransferButton) forControlEvents:UIControlEventEditingChanged];
    
    [self.tfSecretPin addTarget:self action:@selector(updateTransferButton) forControlEvents:UIControlEventEditingChanged];
}

-(void)updateTransferButton{
    BOOL textFieldsIsValid = self.tfToNumber.text.length > 0 && self.tfAmount.text.length >0 && self.tfSecretPin.text.length > 0;
    
    self.btnTransfer.enabled = textFieldsIsValid;
}

@end
