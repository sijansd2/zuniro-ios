//
//  SignUpViewController.h
//  SymlexPro
//
//  Created by admin on 7/18/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ComboView.h"
#import "WhiteBorderedButton.h"
#import "PostDataController.h"
#import "GetDataController.h"
#import "XMLParser.h"

@interface SignUpViewController : UIViewController<UITextFieldDelegate,ComboDelegate,PostDataControllerDelegate,GetDataControllerDelegate,XMLParserDelegate>

@property (weak, nonatomic) IBOutlet WhiteBorderedButton *btnSignUp;
@property (weak, nonatomic) IBOutlet UITextField *tfCountryName;
@property (weak, nonatomic) IBOutlet UITextField *tfNumber;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
@property (weak, nonatomic) IBOutlet UITextField *tfConfirmPassword;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
- (IBAction)btnSignUpPressed:(id)sender;
@end
