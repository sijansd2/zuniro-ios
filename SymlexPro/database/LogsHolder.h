//
//  LogsHolder.h
//  Pak Tel
//
//  Created by Riaz Hasan on 10/11/15.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface LogsHolder : NSObject
-(instancetype)initWithNumber:(NSString *)number Duration:(NSString*)duration CallID:(NSString *)call_id Date:(NSString*)date Call_status:(NSString*)call_status;
@property (nonatomic) int id;
@property (strong, nonatomic) NSString *number;
@property (strong, nonatomic) NSString *Duration;
@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) NSString *call_id;
@property (strong, nonatomic) NSString *call_status;

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) UIImage *image;

@end
